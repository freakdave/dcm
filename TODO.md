For Version 1:

 - Add "name" to submeshes
 - Add "blend_mode" to materials (none, alpha, add, colour, modulate, one_alpha)
 - Add "cull_mode" to the mesh header (none, back, front)
 - Add "bone_format" to the vertex structure (e.g. BONE_FORMAT_1UB, BONE_FORMAT_2UB, BONE_FORMAT_3UB) (number of bones per vertex, and size of the bone index)
 - Add "weight_format" to the vertex structure (eg. VERTEX_WEIGHT_1UB, VERTEX_WEIGHT_2F) (number of weights per vertex, and the format of the weight)
 - Add "joint_count" to the MeshHeader
 - Add joint data
 - Add "animation_count" to the MeshHeader
 - Add animation data (e.g. name, fps, time etc.)
