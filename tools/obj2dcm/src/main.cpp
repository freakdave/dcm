#include <string>
#include <map>
#include <vector>
#include <iostream>
#include <iomanip>
#include <filesystem>
#include <functional>

#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"
#include "dcm.h"
#include "tristripper/tri_stripper.h"
#include "nvstripper/NvTriStrip.h"

typedef std::size_t Index;

bool nvstrip = true;
bool strip = true;

enum ArgType {
    ARG_TYPE_NAMED_OPTIONAL,
    ARG_TYPE_NAMED_REQUIRED,
    ARG_TYPE_FLAG_OPTIONAL,
    ARG_TYPE_FLAG_REQUIRED,
};

static std::map<std::string, std::vector<std::string>> OPTS;

const std::vector<std::tuple<std::string, std::string, std::string, ArgType>> COMMANDS = {
    {"-i", "--input", "path to the input .obj file", ARG_TYPE_NAMED_REQUIRED},
    {"-o", "--output", "output filename", ARG_TYPE_NAMED_REQUIRED},
    {"-h", "--help", "this help screen", ARG_TYPE_FLAG_OPTIONAL},
    {"-n", "--no-strip", "don't generate triangle strips", ARG_TYPE_FLAG_OPTIONAL},
    {"-N", "--use-nvidia-stripper", "use the nvstripper library", ARG_TYPE_FLAG_OPTIONAL},
};

const char* HELP_TEXT_PREAMBLE = R"(
Usage: obj2dcm [OPTION]... -i [WAVEFRONT FILE] -o [OUTPUT]
Convert a Wavefront .obj model into a .dcm (Dreamcast Mesh)

)";

static void print_help() {
    std::cout << HELP_TEXT_PREAMBLE;

    for(auto cmd: COMMANDS) {
        auto a = std::get<0>(cmd);
        auto b = std::get<1>(cmd);
        auto c = std::get<2>(cmd);

        std::cout << "  " << a << ", " << std::left << std::setw(20) << std::setfill(' ') << b  << c << std::endl;
    }

    std::cout << std::endl;
}

static std::string normalize_command(const std::string& arg) {
    for(auto& t: COMMANDS) {
        if(std::get<0>(t) == arg || std::get<1>(t) == arg) {
            return std::get<1>(t);
        }
    }

    return "";
}

/* FIXME: Take required arguments without -- prefixes */
std::map<std::string, std::vector<std::string>> parse_options(int argc, char* argv[], bool* ok) {
    std::map<std::string, std::vector<std::string>> opts;

    assert(ok);

    std::string current_arg;
    for(int i = 1; i < argc; ++i) {
        std::string arg = argv[i];
        if(current_arg.empty()) {
            /* Awaiting command */
            auto l = arg.length();
            if(l < 2 || arg[0] != '-') {
                std::cerr << "Invalid argument: " << arg << std::endl;
                *ok = false;
            }

            auto normalized = normalize_command(arg);
            if(normalized.empty()) {
                std::cerr << "Invalid argument: " << arg << std::endl;
                *ok = false;
            }

            current_arg = normalized.substr(normalized.find_first_not_of("-"), std::string::npos);

            /* Check is valid */
            bool found = false;
            bool is_flag = false;
            for(auto& cmd: COMMANDS) {
                auto name = std::get<1>(cmd);
                if(name == normalized) {
                    found = true;
                    is_flag = std::get<3>(cmd) == ARG_TYPE_FLAG_OPTIONAL || std::get<3>(cmd) == ARG_TYPE_FLAG_REQUIRED;
                    break;
                }
            }

            if(!found) {
                std::cerr << "Invalid argument: " << arg << std::endl;
                exit(10); /* FIXME: Return optional<> and handle in main */
            }

            /* Don't expect a value */
            if(is_flag) {
                auto& values = opts[current_arg];
                values.push_back("true");
                current_arg.clear();
            }

        } else {
            /* Awaiting value */
            auto& values = opts[current_arg];
            values.push_back(arg);
            current_arg.clear();
        }
    }

    *ok = true;
    return opts;
}

#pragma pack(push, 1)

struct Vertex {
    float xyz[3];
    float uv[2];
    uint8_t bgra[4];
    float n[3];
};

#pragma pack(pop)

bool group_triangles(
    const tinyobj::attrib_t& attrib,
    const std::vector<tinyobj::shape_t>& shapes,
    std::vector<Vertex>& vertices,
    std::map<int, std::vector<Index>>& tri_indexes) {

    /* We make a vertex key out of the 3 parts. We store each in 21 bits
     * of the 64 bit key which means we're limited to 2097151 vertices...
     * I think that's OK for our purposes! */
    const int max = 0x1FFFFF;
    auto make_vertex_key = [max](int v, int n, int t) -> uint64_t {
        assert(v < max);
        assert(n < max);
        assert(t < max);

        n = (n == -1) ? max - 1 : n;
        t = (t == -1) ? max - 1 : t;

        return uint64_t(v & 0x1FFFFF) << 42 | uint64_t(n & 0x1FFFFF) << 21 | (t & 0x1FFFFF);
    };

    std::map<uint64_t, Index> vertex_lookup;

    /* OK so we have our inputs. First step is to group our shapes by material
     * we're going to ignore lines and points and tinyobjloader will have done the
     * hard work of triangulating everything. .obj files are annoying and vertex, normal,
     * and tex coord info are separated so we need to combine */

    for(auto& shape: shapes) {
        int k = 0;
        int f = 0;
        for(auto& count: shape.mesh.num_face_vertices) {
            if(count != 3) {
                return false;
            }

            int material = shape.mesh.material_ids[f];

            /* Add the triangle to the material set */
            auto& out = tri_indexes[material];

            for(int j = 0; j < count; ++j, ++k) {
                auto idx = shape.mesh.indices[k];
                auto key = make_vertex_key(idx.vertex_index, idx.normal_index, idx.texcoord_index);
                int vidx = 0;
                if(vertex_lookup.count(key)) {
                    /* We've already seen this vertex */
                    vidx = vertex_lookup.at(key);
                } else {
                    /* New vertex */
                    Vertex newv;

                    if(idx.vertex_index > -1) std::memcpy(newv.xyz, &attrib.vertices[idx.vertex_index * 3], sizeof(float) * 3);
                    if(idx.texcoord_index > -1) std::memcpy(newv.uv, &attrib.texcoords[idx.texcoord_index * 2], sizeof(float) * 2);
                    if(idx.normal_index > -1) std::memcpy(newv.n, &attrib.normals[idx.normal_index * 3], sizeof(float) * 3);

                    if(!attrib.colors.empty()) {
                        newv.bgra[0] = attrib.colors[(idx.vertex_index * 3) + 2] * 255.0f;
                        newv.bgra[1] = attrib.colors[(idx.vertex_index * 3) + 1] * 255.0f;
                        newv.bgra[2] = attrib.colors[(idx.vertex_index * 3)] * 255.0f;
                        newv.bgra[3] = 255;
                    } else {
                        std::memset(newv.bgra, 255, sizeof(newv.bgra));
                    }

                    vidx = vertex_lookup[key] = vertices.size();
                    vertices.push_back(newv);
                }

                out.push_back(vidx);
            }
            ++f;
        }
    }

    return true;
}

struct SubMeshTemp {
    SubMeshArrangement arrangement;
    std::vector<SubMeshVertexRange> ranges;
};

typedef std::shared_ptr<SubMeshTemp> SubMeshPtr;
typedef std::map<SubMeshArrangement, SubMeshPtr> ArrangedSubMesh;


void strip_nvidia(
    const std::vector<Vertex>& vertices,
    const std::vector<Index>& indices,
    std::function<SubMeshPtr (SubMeshArrangement)> get_or_create,
    std::vector<Vertex>& final_vertices) {
    PrimitiveGroup* output = nullptr;
    unsigned short count = 0;
    SetCacheSize(24);
    SetStitchStrips(false);
    GenerateStrips(&indices[0], indices.size(), &output, &count);

    int generated = 0;
    PrimitiveGroup* it = output;
    for(int i = 0; i < count; ++i, ++it) {
        SubMeshPtr submesh;
        if(it->type == PT_LIST) {
            submesh = get_or_create(SUB_MESH_ARRANGEMENT_TRIANGLES);
        } else if(it->type == PT_STRIP) {
            submesh = get_or_create(SUB_MESH_ARRANGEMENT_TRIANGLE_STRIP);
        } else {
            printf("Had a fan :(\n");
        }

        generated += it->numIndices;

        SubMeshVertexRange r;
        r.start = final_vertices.size();
        r.count = it->numIndices;

        for(std::size_t j = 0; j < it->numIndices; ++j) {
            auto idx = it->indices[j];
            final_vertices.push_back(vertices[idx]);
        }

        submesh->ranges.push_back(r);
    }

    delete [] output;
}

void strip_tristripper(
    const std::vector<Vertex>& vertices,
    const std::vector<Index>& indices,
    std::function<SubMeshPtr (SubMeshArrangement)> get_or_create,
    std::vector<Vertex>& final_vertices) {

    triangle_stripper::tri_stripper stripper(indices);
    stripper.SetCacheSize(5000);
    stripper.SetMinStripSize(2);
    triangle_stripper::primitive_vector output;
    stripper.Strip(&output);

    int generated = 0;
    for(auto& group: output) {
        SubMeshPtr submesh;
        if(group.Type == triangle_stripper::primitive_type::TRIANGLES) {
            submesh = get_or_create(SUB_MESH_ARRANGEMENT_TRIANGLES);
        } else {
            submesh = get_or_create(SUB_MESH_ARRANGEMENT_TRIANGLE_STRIP);
        }

        generated += group.Indices.size();

        SubMeshVertexRange r;
        r.start = final_vertices.size();
        r.count = group.Indices.size();

        for(auto& idx: group.Indices) {
            final_vertices.push_back(vertices[idx]);
        }

        submesh->ranges.push_back(r);
    }
}

bool convert(const std::string& input, const std::string& output) {
    tinyobj::ObjReaderConfig reader_config;
    reader_config.mtl_search_path = std::filesystem::path(input).parent_path().string();
    reader_config.triangulate = true;

    tinyobj::ObjReader reader;

    std::string warn;
    std::string err;

    bool ret = reader.ParseFromFile(input, reader_config);
    if(!ret) {
        std::cerr << err << std::endl;
        return false;
    }

    if(!warn.empty()) {
        std::cout << warn << std::endl;
    }

    auto& attrib = reader.GetAttrib();
    auto& shapes = reader.GetShapes();
    auto& materials = reader.GetMaterials();

    std::vector<Vertex> vertices;
    /* Triangles grouped by material */
    std::map<int, std::vector<Index>> tri_indexes;

    if(!group_triangles(attrib, shapes, vertices, tri_indexes)) {
        std::cerr << "There was a problem grouping the triangles" << std::endl;
        return false;
    }

    /* By this point we should have a load of triangles, grouped by material. Now we need
     * to tristrip those triangles before writing them out as submeshes */


    std::vector<Vertex> final_vertices;

    std::map<int, ArrangedSubMesh> material_submeshes;
    int submesh_count = 0;

    auto get_or_create = [&](int mat, SubMeshArrangement arr) -> SubMeshPtr {
        auto& ret = material_submeshes[mat][arr];
        if(!ret) {
            ++submesh_count;
            ret = std::make_shared<SubMeshTemp>();
        }

        return ret;
    };

    bool strip = false;
    if(strip) {
        for(auto& p: tri_indexes) {
            if(nvstrip) {
                strip_nvidia(
                    vertices,
                    p.second,
                    [=](SubMeshArrangement arr) -> SubMeshPtr {
                        return get_or_create(p.first, arr);
                    },
                    final_vertices
                );
            } else {
                strip_tristripper(
                    vertices,
                    p.second,
                    [=](SubMeshArrangement arr) -> SubMeshPtr {
                        return get_or_create(p.first, arr);
                    },
                    final_vertices
                );
            }
        }
    } else {
        for(auto& p: tri_indexes) {
            auto sm = get_or_create(p.first, SUB_MESH_ARRANGEMENT_TRIANGLES);
            auto start = final_vertices.size();
            for(auto& idx: p.second) {
                final_vertices.push_back(vertices[idx]);
            }
            SubMeshVertexRange r;
            r.start = start;
            r.count = p.second.size();
            sm->ranges.push_back(r);
        }
    }

    std::ofstream fout(output, std::ios::binary);
    if(!fout) {
        std::cerr << "Couldn't open file for writing: " << output << std::endl;
        return false;
    }

    uint8_t idx_size = (final_vertices.size() < 256) ? 1 : (final_vertices.size() < 65536) ? 2 : 4;

    FileHeader fheader;
    fheader.id[0] = 'D';
    fheader.id[1] = 'C';
    fheader.id[2] = 'M';
    fheader.version = DCM_CURRENT_VERSION;
    fheader.material_count = tri_indexes.size();
    fheader.pos_format = POSITION_FORMAT_3F;
    fheader.tex0_format = TEX_COORD_FORMAT_2F;
    fheader.color_format = COLOR_FORMAT_4UB;
    fheader.normal_format = NORMAL_FORMAT_3F;
    fheader.tex1_format = TEX_COORD_FORMAT_NONE;
    fheader.offset_colour_format = COLOR_FORMAT_NONE;
    fheader.mesh_count = 1;
    fheader.mesh_offset = sizeof(FileHeader) + (sizeof(Material) * fheader.material_count);
    // Pos, UV, Colour + Normal
    fheader.vertex_size = sizeof(Vertex);
    fheader.index_size = idx_size;
    fheader.reserved0 = fheader.reserved1 = 0;

    fout.write((char*) &fheader, sizeof(FileHeader));

    for(auto& sm: material_submeshes) {
        int mat_id = sm.first;

        auto& mat = materials.at(mat_id);

        Material new_material;

        std::memset(new_material.diffuse_map, 0, 32);
        std::memset(new_material.light_map, 0, 32);
        std::memset(new_material.normal_map, 0, 32);
        std::memset(new_material.specular_map, 0, 32);

        std::strncpy(new_material.name, mat.name.c_str(), 32);
        std::memcpy(new_material.ambient, mat.ambient, sizeof(mat.ambient));
        std::memcpy(new_material.diffuse, mat.diffuse, sizeof(mat.diffuse));
        std::memcpy(new_material.specular, mat.specular, sizeof(mat.specular));
        std::memcpy(new_material.emission, mat.emission, sizeof(mat.emission));

        /* Set the alpha of the material colours to the dissolve level (1 == opaque) */
        new_material.ambient[3] = mat.dissolve;
        new_material.diffuse[3] = mat.dissolve;
        new_material.specular[3] = mat.dissolve;
        new_material.emission[3] = mat.dissolve;

        new_material.shininess = 0.001f * mat.shininess;

        std::strncpy(new_material.diffuse_map, mat.diffuse_texname.c_str(), 32);
        std::strncpy(new_material.normal_map, mat.normal_texname.c_str(), 32);
        std::strncpy(new_material.specular_map, mat.specular_texname.c_str(), 32);

        fout.write((char*) &new_material, sizeof(Material));
    }

    MeshHeader mheader;
    mheader.submesh_count = submesh_count;
    mheader.reserved[0] = mheader.reserved[1] = mheader.reserved[2] = 0;
    mheader.vertex_count = final_vertices.size();
    mheader.first_submesh_offset = fheader.mesh_offset + sizeof(MeshHeader) + (mheader.vertex_count * sizeof(Vertex));
    mheader.next_mesh_offset = 0;

    fout.write((char*) &mheader, sizeof(MeshHeader));
    for(auto& v: final_vertices) {
        fout.write((char*) &v.xyz[0], sizeof(float) * 3);
        fout.write((char*) &v.uv[0], sizeof(float) * 2);
        fout.write((char*) &v.bgra[0], sizeof(uint8_t) * 4);
        fout.write((char*) &v.n[0], sizeof(float) * 3);
    }

    std::size_t last_submesh_offset = fout.tellp();

    std::size_t i = 0;
    for(auto& msm: material_submeshes) {
        for(auto& sm: msm.second) {
            SubMeshHeader smheader;
            smheader.arrangement = sm.first;
            smheader.material_id = msm.first;
            smheader.type = SUB_MESH_TYPE_RANGED;
            smheader.num_ranges_or_indices = sm.second->ranges.size();

            std::size_t data_size = (smheader.type == SUB_MESH_TYPE_RANGED) ?
                sizeof(SubMeshVertexRange) * smheader.num_ranges_or_indices :
                sizeof(uint32_t) * smheader.num_ranges_or_indices;

            smheader.next_submesh_offset = last_submesh_offset + sizeof(SubMeshHeader) + data_size;

            if(++i == material_submeshes.size()) {
                smheader.next_submesh_offset = 0;
            }

            last_submesh_offset = smheader.next_submesh_offset;

            fout.write((char*) &smheader, sizeof(SubMeshHeader));

            if(smheader.type == SUB_MESH_TYPE_RANGED) {
                for(auto& range: sm.second->ranges) {
                    fout.write((char*) &range.start, sizeof(uint32_t));
                    fout.write((char*) &range.count, sizeof(uint32_t));
                }
            }
        }
    }

    fout.flush();
    fout.close();

    return true;
}

int main(int argc, char* argv[]) {
    bool args_ok = false;
    OPTS = parse_options(argc, argv, &args_ok);

    if(!args_ok || OPTS.count("help")) {
        print_help();
        return int(args_ok);
    }

    std::string input = OPTS["input"][0];
    std::string output = OPTS["output"][0];
    strip = OPTS.count("no-strip") && OPTS["no-strip"][0] != "true";
    nvstrip = OPTS.count("use-nvidia-stripper") && OPTS["use-nvidia-stripper"][0] == "true";

    if(!convert(input, output)) {
        return 2;
    }

    return 0;
}
